﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;



public class GameManager : MonoBehaviour
{
    //this puppy will geneate the world, plantes and pickups
    public GameObject worldPrefab;
    public int numWorlds = 20;
    public float mapSize = 50f;
    public GameObject quarkPrefab;
    public int numQuarks = 10;
    //UI texts
    int score = 0;
    public Text scoreText;
    public Text highScoreText;
    //do we want to generate the world or use a pre-made one
    public bool isGeneratingWorld = true;
    //the safe distance away from player
    public float safeDistance = 20f;
    int PlayerHighScore = 0;

    //find the player so we can get safezone spawn
    GameObject player;

    public float spawnTime = 5f;
    float spawnTimeCounter = 0f;


    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        if (isGeneratingWorld)
        {
            GenerateWorld();
        }
        if (!PlayerPrefs.HasKey("Score"))
        {
            PlayerPrefs.SetInt("Score", score);
            PlayerHighScore = score;
        }
        else
        {
            PlayerHighScore = PlayerPrefs.GetInt("Score");
        }
        highScoreText.text = "High Score: " + PlayerHighScore.ToString();

    }




    void ClearWorld()
    {
        //remove all the collectables and worlds from the 
        GameObject[] worlds = GameObject.FindGameObjectsWithTag("World");
        GameObject[] quarks = GameObject.FindGameObjectsWithTag("Collectable");
        foreach (var item in worlds)
        {
            Destroy(item);
        }
        foreach (var item in quarks)
        {
            Destroy(item);
        }
    }


    void GenerateWorld()
    {
        for (int i = 0; i < numWorlds; i++)
        {
            AddWorld();
        }
        for (int i = 0; i < numQuarks; i++)
        {
            AddQuark();
        }
    }

    void AddQuark()
    {
        Instantiate(quarkPrefab, GetRandomPos(), Quaternion.identity);
    }

    void AddWorld()
    {
        float randomMass = UnityEngine.Random.Range(0.5f, 2f);
        GameObject world = Instantiate(worldPrefab, GetRandomPos(), Quaternion.identity) as GameObject;
        //now force world creation (not done on start so we can set values before generating world
        world.GetComponent<World>().CreateWorld(randomMass, (int)(5 * randomMass));
    }


    Vector3 GetRandomPos()
    {
        if (!player)
            return Vector3.zero;
        //write some safe zone in so we dont spawn stuff on top of player
        float xpos = UnityEngine.Random.Range(-mapSize, mapSize);
        float zpos = UnityEngine.Random.Range(-mapSize, mapSize);
        //see if it is safe, if not run again!!
        if (Vector3.Distance(player.transform.position, new Vector3(xpos, 0, zpos)) < safeDistance)
        {
            //danger- recursive! we really shoould check this doesn't run too many times.....
            return GetRandomPos();
        }
        else
        {
            return new Vector3(xpos, 0, zpos);
        }
       
    }

    void Collected()
    {
        //the collectable script will run this function and it will add one to score
        score++;
    }

	
    // Update is called once per frame
    void Update()
    {
        //set the canvas text
        scoreText.text = "Score: " + score;
        //every few seconds add another quark and add another world
        spawnTimeCounter += Time.deltaTime;
        if (spawnTimeCounter > spawnTime)
        {
            spawnTimeCounter = 0f;
            AddQuark();
            AddWorld();
        }
    }

    public void PlayerKilled()
    {
        //current high score
        if (score > PlayerPrefs.GetInt("Score"))
        {
            PlayerPrefs.SetInt("Score", score);
        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

}
