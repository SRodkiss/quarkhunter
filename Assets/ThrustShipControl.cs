﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ThrustShipControl : MonoBehaviour
{

    Rigidbody rb;
    public float turnForce = 100f;
    public float thrustForce = 10f;

    //the particle systems
    public ParticleSystem LeftParticleSystem;
    public ParticleSystem RightParticleSystem;
    ParticleSystem.EmissionModule leftEmitter;
    ParticleSystem.EmissionModule rightEmitter;

    AudioSource audioSource;


    //explosion
    public GameObject explosionPrefab;

    GameObject gameManager;


    // Use this for initialization
    void Start()
    {
        gameManager = GameObject.FindGameObjectWithTag("GameController");
        rb = GetComponent<Rigidbody>();
        leftEmitter = LeftParticleSystem.emission;
        rightEmitter = RightParticleSystem.emission;
        audioSource = GetComponent<AudioSource>();
        audioSource.volume = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        UpdateControl();
    }

    void UpdateControl()
    {
        float xinput = Input.GetAxis("Horizontal") * turnForce;
        rb.AddTorque(0, xinput, 0);
        //thrust
        if (Input.GetButton("Jump"))
        {
            rb.AddForce(transform.forward * thrustForce);
            //
            leftEmitter.rateOverTime = 50f;
            rightEmitter.rateOverTime = 50f;
            audioSource.volume = Mathf.SmoothStep(audioSource.volume, 1f, Time.deltaTime * 20f);
        }
        else
        {
            leftEmitter.rateOverTime = 0f;
            rightEmitter.rateOverTime = 0f;
            audioSource.volume = Mathf.SmoothStep(audioSource.volume, 0f, Time.deltaTime * 20f);
        }
        //an escape key- just kill the player!!
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //we hit something
            gameManager.SendMessage("PlayerKilled");
            Instantiate(explosionPrefab, transform.position, transform.rotation);
            Destroy(gameObject, 0.25f);
        }

    }

    //collision with the atoms
    void OnCollisionEnter(Collision collision)
    {
        //we hit something
        gameManager.SendMessage("PlayerKilled");
        Instantiate(explosionPrefab, transform.position, transform.rotation);
        Destroy(gameObject, 0.25f);
    }

}
