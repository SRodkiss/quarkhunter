﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class World : MonoBehaviour
{

    public float mass = 1f;
    public int numElectrons = 2;
    public GameObject electronPrefab;
    public float electronDistance = 2f;
    GameManager gameManager;

    // Use this for initialization
    void Start()
    {
        //get the gamecontroller
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        //don't make mass/scale and electrons on start if the gamemanager will do that!!
        if (!gameManager.isGeneratingWorld)
        {
            CreateWorld(mass, numElectrons);
        }
    }

    //create the world based on the parameters- set its mass, scale and instantiate the elcetrons
    public void CreateWorld(float newMass, int newNumElectrons)
    {
        //set the params
        mass = newMass;
        numElectrons = newNumElectrons;
        //set the scale based on mass
        transform.localScale = new Vector3(mass / 2f, mass / 2f, mass / 2f);
        //set the distance of the electons based on mass
        electronDistance = mass;
        //instantiate all the electrons for this object
        for (int i = 0; i < numElectrons; i++)
        {
            GameObject electron = Instantiate(electronPrefab, transform.position, transform.rotation) as GameObject;
            electron.SendMessage("SetDistance", electronDistance);
        }   
    }
	
    // Update is called once per frame
    void Update()
    {
		
    }
}
