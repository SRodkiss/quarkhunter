﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//this class controle the UI and holds functions to start/restart level and pause
public class UIManager : MonoBehaviour
{
    public enum GameState
    {
        ready,
        playing,
        paused
    }

    public GameState gameState = GameState.ready;


    public GameObject startMenuCanvas;
    public GameObject gameCanvas;
    public GameObject endGameCanvas;


    public void StartGame()
    {
        Time.timeScale = 1.0f;
        startMenuCanvas.SetActive(false);
        gameCanvas.SetActive(true);
        endGameCanvas.SetActive(false);
    }


    public void RestartGame()
    {
        StartGame();
    }


    // Use this for initialization
    void Start()
    {
        Time.timeScale = 0f;
        startMenuCanvas.SetActive(true);
        gameCanvas.SetActive(false);
        endGameCanvas.SetActive(false);
    }

    public void PlayerKilled()
    {
        //switch to main menu
        startMenuCanvas.SetActive(false);
        gameCanvas.SetActive(false);
        endGameCanvas.SetActive(true);
    }
	
    // Update is called once per frame
    void Update()
    {   

		
    }
}
