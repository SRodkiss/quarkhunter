﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{

    public GameObject target;
    public Vector3 offset = new Vector3(0, 10, 0);

    // Update is called once per frame
    void Update()
    {
        if (target)
        {
            transform.position = target.transform.position + offset;
            transform.LookAt(target.transform);
        }
 
    }
}
