﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class PlanetGravity : MonoBehaviour
{

    GameObject[] worlds;
    public float gravityConstant = 0.6f;
    Rigidbody rb;

    float threshold = 0.05f;

    // Use this for initialization
    void Start()
    {
        //gather array of gameobjects with the world tag
        worlds = GameObject.FindGameObjectsWithTag("World");
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 totalForce = Vector3.zero;
        foreach (var world in worlds)
        {
            totalForce += CalculateGravity(world);
        }
        //apply the force
        rb.AddForce(totalForce);
    }

    Vector3 CalculateGravity(GameObject world)
    {
        //f = gravityconstant * ( (mass1 * mass2) / distance *distance)
        Vector3 direction = (world.transform.position - transform.position).normalized;
        float distance = Vector3.Distance(transform.position, world.transform.position);
        float force = gravityConstant * (world.GetComponent<World>().mass / (distance * distance));
        if (force > threshold)
        {
            //return the force
            return direction * force;
        }
        else
        {
            //return zero
            return Vector3.zero;
        }

    }

}
