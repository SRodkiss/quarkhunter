﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrierBounce : MonoBehaviour
{
    //bounce any rigid body back into the z direction it's facing on trigger stay
    //force to apply
    public float force = 100f;


    void OnTriggerStay(Collider other)
    {
        //apply a force to the other's rigidbody in the z direction of the 
        Vector3 pushForce = force * transform.forward;
        other.attachedRigidbody.AddForce(pushForce);
    }


    // Use this for initialization
    void Start()
    {
		
    }
	
    // Update is called once per frame
    void Update()
    {
		
    }
}
