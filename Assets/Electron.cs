﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Electron : MonoBehaviour
{

    public float distance = 2f;
    //the child visual mesh
    public GameObject visual;
    //the rotation to move every frame
    public float rotationSpeed = 10f;

    // Use this for initialization
    void Start()
    {
        SetDistance(distance);
    }

    void SetDistance(float d)
    {
        //set the distance
        distance = d;
        visual.transform.localPosition = new Vector3(0, 0, distance);
        //randomise the rotation
        Quaternion rq = Random.rotation;
        transform.rotation = rq;
    }

	
    // Update is called once per frame
    void Update()
    {
        //rotate it around ocal x- which should be random by the rotation speed
        transform.Rotate(new Vector3(rotationSpeed * Time.deltaTime, 0, 0), Space.Self);
    }
}
