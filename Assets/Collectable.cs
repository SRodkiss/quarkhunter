﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{

    //find the gamecontroller object
    GameObject gameController;

    // Use this for initialization
    void Start()
    {
        gameController = GameObject.FindGameObjectWithTag("GameController");
    }
	
    // Update is called once per frame
    void Update()
    {
		
    }

    void OnTriggerEnter(Collider other)
    {
        //we have been collected
        if (other.tag == "Player")
        {
            //we have been collected
            gameController.SendMessage("Collected");
            Destroy(gameObject);
        }

    }


}
